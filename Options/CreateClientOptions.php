<?php

namespace DSYBSaleClient\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateClientOptions.
 */
class CreateClientOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        // $resolver->setDefaults(array(
        //     'accessToken' => null,
        // ));

        $resolver->setRequired('accessToken')->setAllowedTypes('accessToken', ['string']);
    }
}
