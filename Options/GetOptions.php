<?php 

namespace DSYBSaleClient\Options;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetOptions
 */
class GetOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'recursive' => false,
            'offset' => 0,
            'limit' => 25,
            'expand' => null,
        ));
        
        $resolver->setAllowedTypes('recursive', array('boolean'));
        $resolver->setAllowedTypes('offset', array('int'));
        $resolver->setAllowedTypes('limit', array('int'));
        $resolver->setAllowedTypes('expand', ['null', 'array']);
    }
}
