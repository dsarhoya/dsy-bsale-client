<?php

namespace DSYBSaleClient\Statistics\Options;

use DSYBSaleClient\Options\BaseOptions;
use DSYBSaleClient\Statistics\GroupByOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetDocumentsStatisticsOptions.
 */
class GetDocumentsStatisticsOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'officeIds' => null,
            'siiCodes' => null,
            'productIds' => null,
            'groupBy' => null,
            'fromTimestamp' => null,
            'toTimestamp' => null,
        ]);

        $resolver->setRequired('groupBy');
        $resolver->setAllowedTypes('officeIds', ['null', 'array', 'int']);
        $resolver->setAllowedTypes('siiCodes', ['null', 'array', 'int']);
        $resolver->setAllowedTypes('productIds', ['null', 'array', 'int']);
        $resolver->setAllowedTypes('fromTimestamp', ['null', 'int']);
        $resolver->setAllowedTypes('toTimestamp', ['null', 'int']);
        $resolver->setAllowedTypes('groupBy', ['string'])->setAllowedValues('groupBy', function ($value) {
            if ($value === GroupByOptions::PRODUCTS) {
                return true;
            }
            return false;
        });
    }
}
