<?php

namespace DSYBSaleClient\Statistics;

use DateTime;
use DSYBSaleClient\Client;
use DSYBSaleClient\Elements\Options\Documents\GetDocumentsOptions;
use DSYBSaleClient\Elements\Options\Variants\GetVariantOptions;

/**
 * StatisticDocuments.
 */
class Documents
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getSum(Options\GetDocumentsStatisticsOptions $statisticOptions) : ?array
    {
        $documentsOptions = $this->_getDocumentsOptions($statisticOptions);

        $documents = $this->client->documents->getDocuments($documentsOptions);

        if (count($documents)) {
            return $this->sumDocuments($documents, $statisticOptions->groupBy);
        }

        return null;
    }

    private function _getDocumentsOptions(Options\GetDocumentsStatisticsOptions $statisticOptions): GetDocumentsOptions
    {
        $documentsOptions = [];
        if (null !== $statisticOptions->fromTimestamp) {
            $documentsOptions['rangeDate'][0] = $statisticOptions->fromTimestamp;
        }

        if (null !== $statisticOptions->toTimestamp) {
            $documentsOptions['rangeDate'][1] = $statisticOptions->toTimestamp;
        }

        if (isset($documentsOptions['rangeDate'])) {
            if (!isset($documentsOptions['rangeDate'][0])) {
                $documentsOptions['rangeDate'][0] = 0;
            }

            if (!isset($documentsOptions['rangeDate'][1])) {
                $now = new DateTime();
                $documentsOptions['rangeDate'][1] = $now->getTimestamp();
            }
        }

        if (null !== $statisticOptions->officeIds) {
            if (!is_array($statisticOptions->officeIds)) {
                $documentsOptions['officeId'] = $statisticOptions->officeIds;
            }
        }

        if (null !== $statisticOptions->siiCodes) {
            if (!is_array($statisticOptions->siiCodes)) {
                $documentsOptions['codeSii'] = $statisticOptions->siiCodes;
            }
        }

        $documentsOptions['expand'] = ['details'];
        $documentsOptions['recursive'] = true;
        $documentsOptions['limit'] = 100;

        return new GetDocumentsOptions($documentsOptions);
    }

    private function sumDocuments($documents, $groupBy) : array
    {   
        $result = [];
        if (GroupByOptions::PRODUCTS === $groupBy) {
            $result = $this->groupDocumentsByProducts($documents);
        }
        return $result;
    }

    private function groupDocumentsByProducts($documents) : array
    {
        $result = [];
        $consultedVariants = [];

        foreach ($documents as $document) {
            foreach ($document['details']['items'] as $itemDetail) {
                // NO PUEDO EXPANDIR HASTA VER EL PRODUCTO SOLO CON EL DETALLE. POR ESO CONSULTO LA VARIANTE
                if (!array_key_exists($itemDetail['variant']['id'], $consultedVariants)) {
                    $variant = $this->client->variants->getVariant(new GetVariantOptions([
                        'variantId' => $itemDetail['variant']['id'], 
                        'expand' => ['product']])
                    );

                    $consultedVariants[$variant['id']]['product']['id'] = $variant['product']['id'];
                    $consultedVariants[$variant['id']]['product']['name'] = $variant['product']['name'];
                } else {
                    $variant = $consultedVariants[$itemDetail['variant']['id']];
                }

                if (!array_key_exists($variant['product']['id'], $result)) {
                    $result[$variant['product']['id']] = ['product_name' => $variant['product']['name'], 'amount' => $itemDetail['totalAmount']];
                } else {
                    $result[$variant['product']['id']]['amount'] += $itemDetail['totalAmount'];
                }

            }
        }

        return $result;
    }
}
