<?php

namespace DSYBSaleClient;

use DSYBSaleClient\Elements\Payments;
use DSYBSaleClient\Elements\Shippings;
use GuzzleHttp\Client as GuzzleClient;
use DSYBSaleClient\Options\CreateClientOptions;

/**
 * Client.
 */
class Client
{
    /**
     * @var Elements\Documents
     */
    public $documents;

    /**
     * @var Elements\DocumentTypes
     */
    public $documentTypes;

    /**
     * @var Elements\Refunds
     */
    public $refunds;

    /**
     * @var Statistics\Documents
     */
    public $documentsStatistics;

    /**
     * @var Elements\Variants
     */
    public $variants;

    /** @var Payments */
    public $payments;

    /** @var Shippings */
    public $shippings;

    public function __construct(CreateClientOptions $options)
    {
        $guzzle = new GuzzleClient([
            'base_uri' => 'https://api.bsale.cl',
            'headers' => ['access_token' => $options->accessToken],
        ]);

        $this->documents = new Elements\Documents($guzzle);
        $this->documentTypes = new Elements\DocumentTypes($guzzle);
        $this->refunds = new Elements\Refunds($guzzle);
        $this->payments = new Payments($guzzle);
        $this->shippings = new Shippings($guzzle);

        $this->variants = new Elements\Variants($guzzle);

        $this->documentsStatistics = new Statistics\Documents($this);
    }
}
