<?php 

namespace DSYBSaleClient\Elements;

use GuzzleHttp\Client;

/**
 * AbstractElements
 */
class AbstractElements
{
    /**
     * @var Client
     */
    protected $httpClient;
    
    public function __construct(Client $httpClient){
        $this->httpClient = $httpClient;
    }
    
    protected function get($path, $query = [], $recursive = false){
        $res = (string)$this->httpClient->get($path, ['query' => $query])->getBody();
        $arr = \GuzzleHttp\json_decode($res, true);
        if (false === $recursive||!isset($arr['next'])) {
            return $arr['items'];
        }
        $query['offset'] = $query['offset'] + $query['limit'];
        return array_merge($arr['items'], $this->get($path, $query, $recursive));
    }
}
