<?php 

namespace DSYBSaleClient\Elements\Options\Variants;

use DSYBSaleClient\Options\GetOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetVariantOptions
 */
class GetVariantOptions extends GetOptions
{
    public function configureOptions(OptionsResolver $resolver){
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'variantId' => null,
        ));
        $resolver->setRequired('variantId')->setAllowedTypes('variantId', ['int']);
    }
}
