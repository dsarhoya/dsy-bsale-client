<?php

namespace DSYBSaleClient\Elements\Options\Payments;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateRefundOptions.
 */
class CreatePaymentOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);

        $resolver->setRequired('paymentTypeId')->setAllowedTypes('paymentTypeId', ['string']);
        $resolver->setRequired('amount')->setAllowedTypes('amount', ['int']);
        $resolver->setRequired('documentId')->setAllowedTypes('documentId', ['int']);
        $resolver->setRequired('recordDate')->setAllowedTypes('recordDate', ['int']);
    }
}
