<?php

namespace DSYBSaleClient\Elements\Options\Shippings;

use DSYBSaleClient\Elements\Options\Documents\CreateDocumentOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateShippingOptions.
 */
class CreateShippingOptions extends CreateDocumentOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'declareSii' => 1,
            'clientRut' => null,
            'clientActivity' => null,
            'clientName' => null,
            'clientCity' => null,
            'clientAddress' => null,
            'clientMunicipality' => null,
        ]);

        $resolver->setRequired('documentTypeId')->setAllowedTypes('documentTypeId', ['int']);
        $resolver->setRequired('officeId')->setAllowedTypes('officeId', ['int']);
        $resolver->setRequired('emissionDate')->setAllowedTypes('emissionDate', ['int']);
        $resolver->setRequired('shippingTypeId')->setAllowedTypes('shippingTypeId', ['int']);
        $resolver->setRequired('municipality')->setAllowedTypes('municipality', ['string']);
        $resolver->setRequired('city')->setAllowedTypes('city', ['null', 'string']);
        $resolver->setRequired('address')->setAllowedTypes('address', ['string']);
        $resolver->setRequired('declareSii')->setAllowedTypes('declareSii', ['int']);
        $resolver->setRequired('recipient')->setAllowedTypes('recipient', ['string']);
        $resolver->setAllowedTypes('clientRut', ['null', 'string']);
        $resolver->setAllowedTypes('clientActivity', ['null', 'string']);
        $resolver->setAllowedTypes('clientName', ['null', 'string']);
        $resolver->setAllowedTypes('clientCity', ['null', 'string']);
        $resolver->setAllowedTypes('clientAddress', ['null', 'string']);
        $resolver->setAllowedTypes('clientMunicipality', ['null', 'string']);
        $resolver->setRequired('details')->setAllowedTypes('details', ['array'])->setAllowedValues('details', function ($value) {
            foreach ($value as $item) {
                if (!($item instanceof CreateShippingDetailOptions)) {
                    return false;
                }
            }

            return true;
        });
    }

    public function getDetailsArray(): array
    {
        $details = [];
        foreach ($this->details as $item) {
            $aux = [
                'netUnitValue' => $item->netUnitValue,
                'quantity' => $item->quantity,
                'comment' => $item->comment,
            ];

            if (null != $item->code) {
                $aux = array_merge($aux, ['code' => $item->code]);
            }

            if (null !== $item->taxes) {
                $aux['taxes'] = $item->taxes;
            }

            $details[] = $aux;
        }

        return $details;
    }
}
