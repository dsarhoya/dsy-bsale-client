<?php

namespace DSYBSaleClient\Elements\Options;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentDetailOptions.
 */
class CreateDocumentDetailOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'discount' => null,
            'code' => null,
            'variantId' => null,
            'taxes' => [['code'=>14,'percentage'=>19]],
        ]);

        $resolver->setRequired('netUnitValue')->setAllowedTypes('netUnitValue', ['int', 'float', 'double']);
        $resolver->setRequired('quantity')->setAllowedTypes('quantity', ['int', 'float']);
        $resolver->setRequired('comment')->setAllowedTypes('comment', ['string']);
        $resolver->setAllowedTypes('discount', ['null', 'float']);
        $resolver->setAllowedTypes('taxes', ['null', 'array']);
        $resolver->setAllowedTypes('code', ['null', 'string']);
        $resolver->setAllowedTypes('variantId', ['null', 'int']);
    }
}
