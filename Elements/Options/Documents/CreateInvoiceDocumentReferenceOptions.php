<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentDetailOptions.
 */
class CreateInvoiceDocumentReferenceOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);

        $resolver->setRequired('number')->setAllowedTypes('number', ['string']);
        $resolver->setRequired('referenceDate')->setAllowedTypes('referenceDate', ['int']);
        $resolver->setRequired('reason')->setAllowedTypes('reason', ['string']);
        $resolver->setRequired('codeSii')->setAllowedTypes('codeSii', ['int']);
    }

    public function getAsArray(): array
    {
        return [
            'number' => $this->number,
            'referenceDate' => $this->referenceDate,
            'reason' => $this->reason,
            'codeSii' => $this->codeSii,
        ];
    }
}
