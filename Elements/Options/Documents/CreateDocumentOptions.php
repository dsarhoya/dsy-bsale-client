<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentOptions.
 */
class CreateDocumentOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'documentTypeId' => null,
            'documentSiiCode' => null,
            'dynamicAttributes' => null,
        ]);

        $resolver->setAllowedTypes('documentTypeId', ['null', 'int']);
        $resolver->setAllowedTypes('documentSiiCode', ['null', 'int']);

        $resolver->setAllowedTypes('dynamicAttributes', ['null', 'array'])->setAllowedValues('dynamicAttributes', function ($value) {
            if (null == $value) {
                return true;
            }

            foreach ($value as $item) {
                if (!($item instanceof CreateDocumentDynamicAtributeOptions)) {
                    return false;
                }
            }

            return true;
        });
    }
}
