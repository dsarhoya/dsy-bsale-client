<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Options\GetOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetDocumentsOptions.
 */
class GetDocumentsOptions extends GetOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'codeSii' => null,
            'rangeDate' => null,
            'officeId' => null,
            'number' => null,
        ]);
        $resolver->setAllowedTypes('codeSii', ['null', 'int']);
        $resolver->setAllowedTypes('rangeDate', ['null', 'array']);
        $resolver->setAllowedTypes('officeId', ['null', 'int']);
        $resolver->setAllowedTypes('number', ['null', 'string']);
    }
}
