<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Elements\Options\CreateDocumentDetailOptions;
use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentDetailOptions.
 */
class CreateInvoiceDocumentDetailOptions extends CreateDocumentDetailOptions
{
}
