<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentDetailOptions.
 */
class CreateDocumentDynamicAtributeOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);

        $resolver->setRequired('description')->setAllowedTypes('description', ['string']);
        $resolver->setRequired('dynamicAttributeId')->setAllowedTypes('dynamicAttributeId', ['int']);
    }

    public function getAsArray(): array
    {
        return [
            'description' => $this->description,
            'dynamicAttributeId' => $this->dynamicAttributeId,
        ];
    }
}
