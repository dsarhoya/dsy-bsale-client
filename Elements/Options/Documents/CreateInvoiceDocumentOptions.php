<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateInvoiceDocumentOptions.
 */
class CreateInvoiceDocumentOptions extends CreateDocumentOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'emissionDateTimestamp' => null,
            'expirationDateTimestamp' => null,
            'clientName' => null,
            'clientCity' => null,
            'clientAddress' => null,
            'clientMunicipality' => null,
            'documentSiiCode' => null,
            'clientRut' => null,
            'clientActivity' => null,
            'officeId' => null,
            'references' => null,
            'dispatch' => null,
            'payments' => null,
            'salesConditionId' => null,
            'sellerId' => null,
        ]);

        $resolver->setAllowedTypes('emissionDateTimestamp', ['null', 'int']);
        $resolver->setAllowedTypes('expirationDateTimestamp', ['null', 'int']);
        $resolver->setAllowedTypes('clientRut', ['null', 'string']);
        $resolver->setAllowedTypes('clientActivity', ['null', 'string']);
        $resolver->setAllowedTypes('clientName', ['null', 'string']);
        $resolver->setAllowedTypes('clientCity', ['null', 'string']);
        $resolver->setAllowedTypes('clientAddress', ['null', 'string']);
        $resolver->setAllowedTypes('clientMunicipality', ['null', 'string']);
        $resolver->setAllowedTypes('officeId', ['null', 'int']);
        $resolver->setAllowedTypes('dispatch', ['null', 'bool']);
        $resolver->setAllowedTypes('salesConditionId', ['null', 'int']);
        $resolver->setAllowedTypes('sellerId', ['null', 'int']);
        $resolver->setRequired('details')->setAllowedTypes('details', ['array'])->setAllowedValues('details', function ($value) {
            foreach ($value as $item) {
                if (!($item instanceof CreateInvoiceDocumentDetailOptions)) {
                    return false;
                }
            }

            return true;
        });
        $resolver->setAllowedTypes('references', ['null', 'array'])->setAllowedValues('references', function ($value) {
            if (null == $value) {
                return true;
            }

            foreach ($value as $item) {
                if (!($item instanceof CreateInvoiceDocumentReferenceOptions)) {
                    return false;
                }
            }

            return true;
        });

        $resolver->setAllowedTypes('payments', ['null', 'array'])->setAllowedValues('payments', function ($value) {
            if (null == $value) {
                return true;
            }

            foreach ($value as $item) {
                if (!($item instanceof CreateDocumentPaymentsOptions)) {
                    return false;
                }
            }

            return true;
        });
    }
}
