<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentPaymentsOptions.
 */
class CreateDocumentPaymentsOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);

        $resolver->setRequired('paymentTypeId')->setAllowedTypes('paymentTypeId', ['int']);
        $resolver->setRequired('amount')->setAllowedTypes('amount', ['int']);
        $resolver->setRequired('recordDate')->setAllowedTypes('recordDate', ['int']);
    }

    public function getAsArray(): array
    {
        return [
            'paymentTypeId' => $this->paymentTypeId,
            'amount' => $this->amount,
            'recordDate' => $this->recordDate,
        ];
    }
}
