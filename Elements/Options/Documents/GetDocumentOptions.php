<?php

namespace DSYBSaleClient\Elements\Options\Documents;

use DSYBSaleClient\Options\GetOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetDocumentsOptions.
 */
class GetDocumentOptions extends GetOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'documentId' => null,
        ]);
        $resolver->setRequired('documentId')->setAllowedTypes('documentId', ['int']);
    }
}
