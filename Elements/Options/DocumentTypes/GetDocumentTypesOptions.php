<?php 

namespace DSYBSaleClient\Elements\Options\DocumentTypes;

use DSYBSaleClient\Options\GetOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetDocumentTypesOptions
 */
class GetDocumentTypesOptions extends GetOptions
{
    
}
