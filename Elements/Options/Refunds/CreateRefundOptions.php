<?php

namespace DSYBSaleClient\Elements\Options\Refunds;

use DSYBSaleClient\Options\BaseOptions;
use DSYBSaleClient\SiiDocumentCodes;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateRefundOptions.
 */
class CreateRefundOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'documentTypeId' => null,
            'documentSiiCode' => SiiDocumentCodes::NOTA_DE_CREDITO_ELECTRONICA,
            'emissionDateTimestamp' => null,
            'expirationDateTimestamp' => null,
            'clientName' => null,
            'details' => null,
            // 'clientCity' => null,
            // 'clientAddress' => null,
            // 'clientMunicipality' => null,
        ]);

        $resolver->setAllowedTypes('documentTypeId', ['null', 'int']);
        $resolver->setAllowedTypes('documentSiiCode', ['int']);
        $resolver->setAllowedTypes('emissionDateTimestamp', ['null', 'int']);
        $resolver->setAllowedTypes('expirationDateTimestamp', ['null', 'int']);
        $resolver->setRequired('referenceDocumentId')->setAllowedTypes('referenceDocumentId', ['int'])->setAllowedValues('details', [0, 1]);
        $resolver->setRequired('priceAdjustment')->setAllowedTypes('priceAdjustment', ['int'])->setAllowedValues('details', [0, 1]);
        $resolver->setRequired('editTexts')->setAllowedTypes('editTexts', ['int']);
        $resolver->setRequired('officeId')->setAllowedTypes('officeId', ['int']);
        $resolver->setRequired('motive')->setAllowedTypes('motive', ['string']);
        $resolver->setRequired('clientRut')->setAllowedTypes('clientRut', ['string']);
        $resolver->setRequired('clientActivity')->setAllowedTypes('clientActivity', ['null', 'string']);
        $resolver->setRequired('clientCity')->setAllowedTypes('clientCity', ['string']);
        $resolver->setRequired('clientAddress')->setAllowedTypes('clientAddress', ['string']);
        $resolver->setRequired('clientMunicipality')->setAllowedTypes('clientMunicipality', ['string']);
        $resolver->setRequired('details')->setAllowedTypes('details', ['array'])->setAllowedValues('details', function ($value) {
            foreach ($value as $item) {
                if (!($item instanceof CreateRefundDocumentDetailOptions)) {
                    return false;
                }
            }

            return true;
        });
    }
}
