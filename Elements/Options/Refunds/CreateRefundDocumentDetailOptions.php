<?php

namespace DSYBSaleClient\Elements\Options\Refunds;

use DSYBSaleClient\Options\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateRefundDocumentDetailOptions.
 */
class CreateRefundDocumentDetailOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);

        $resolver->setRequired('documentDetailId')->setAllowedTypes('documentDetailId', ['int']);
        $resolver->setRequired('quantity')->setAllowedTypes('quantity', ['float', 'int']);
        $resolver->setRequired('unitValue')->setAllowedTypes('unitValue', ['float', 'int']);
    }
}
