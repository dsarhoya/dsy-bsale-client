<?php 

namespace DSYBSaleClient\Elements;

use DSYBSaleClient\Elements\AbstractElements;
use DSYBSaleClient\Elements\Options\DocumentTypes as Options;

/**
 * DocumentTypes
 */
class DocumentTypes extends AbstractElements
{
    public function getDocumentTypes(Options\GetDocumentTypesOptions $options){
        return $this->get('/v1/document_types.json', [
            'limit' => $options->limit,
            'offset' => $options->offset,
        ], $options->recursive);
    }
}
