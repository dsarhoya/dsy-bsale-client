<?php

namespace DSYBSaleClient\Elements;

use DSYBSaleClient\Elements\Options\Documents\CreateDocumentDynamicAtributeOptions;
use DSYBSaleClient\Elements\Options\Shippings\CreateShippingOptions;

/**
 * Shippings.
 */
class Shippings extends AbstractElements
{
    public function getPostShippingBodyArray(CreateShippingOptions $options): array
    {
        $optionsArr = [
            'documentTypeId' => $options->documentTypeId,
            'officeId' => $options->officeId,
            'emissionDate' => $options->emissionDate,
            'shippingTypeId' => $options->shippingTypeId,
            'municipality' => $options->municipality,
            'city' => $options->city,
            'address' => $options->address,
            'declareSii' => $options->declareSii,
            'recipient' => $options->recipient,
            'details' => $options->getDetailsArray(),
            'client' => [
                'code' => $options->clientRut,
                'company' => $options->clientName,
                'activity' => $options->clientActivity,
                'address' => $options->clientAddress,
                'municipality' => $options->clientLocality,
            ],
        ];

        if (null != $options->dynamicAttributes && 0 < count($options->dynamicAttributes)) {
            $optionsArr['dynamicAttributes'] = array_map(function (CreateDocumentDynamicAtributeOptions $options) {
                return $options->getAsArray();
            }, $options->dynamicAttributes);
        }

        return $optionsArr;
    }

    public function postShipping(string $bodyString): array
    {
        $request = $this->httpClient->post('/v1/shippings.json', ['body' => $bodyString]);
        $res = (string) $request->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }

    public function postShippingAsArray(array $body): array
    {
        $request = $this->httpClient->post('/v1/shippings.json', ['json' => $body]);
        $res = (string) $request->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }

    public function createShipping(CreateShippingOptions $options)
    {
        $optionsArr = $this->getPostShippingBodyArray($options);

        return $this->postShipping(json_encode($optionsArr));
    }
}
