<?php

namespace DSYBSaleClient\Elements;

use DSYBSaleClient\Elements\Options\Variants as Options;

/**
 * Variants.
 */
class Variants extends AbstractElements
{
    public function getVariant(Options\GetVariantOptions $options)
    {
        $query = [];
        if (null !== $options->expand) {
            $query['expand'] = '['.(implode(',', $options->expand)).']';
        }
        $res = (string) $this->httpClient->get("/v1/variants/{$options->variantId}.json", ['query' => $query])->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }
}
