<?php

namespace DSYBSaleClient\Elements;

use DSYBSaleClient\Elements\Options\Documents as DocumentOptions;
use DSYBSaleClient\Elements\Options\Refunds as Options;

/**
 * Documents.
 */
class Refunds extends AbstractElements
{
    // public function getDocuments(Options\GetDocumentsOptions $options){
    //     return $this->get('/v1/documents.json', [
    //         'limit' => $options->limit,
    //         'offset' => $options->offset,
    //     ], $options->recursive);
    // }

    public function getRefund(DocumentOptions\GetDocumentOptions $options)
    {
        $query = [];
        if (null !== $options->expand) {
            $query['expand'] = '['.(implode(',', $options->expand)).']';
        }
        $res = (string) $this->httpClient->get("/v1/returns/{$options->documentId}.json", ['query' => $query])->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }

    public function createRefund(Options\CreateRefundOptions $options)
    {
        /*
        "documentTypeId": 9,
        "officeId": 1,
        "referenceDocumentId": 11528,
        "expirationDate": 1407384000,
        "emissionDate": 1407384000,
        "motive": "prueba api",
        "declareSii": 1,
        "priceAdjustment": 0,
        "editTexts": 0,
        "type": 1
         */

        $optionsArr = [
            'referenceDocumentId' => $options->referenceDocumentId,
            'officeId' => $options->officeId,
            'priceAdjustment' => $options->priceAdjustment,
            'editTexts' => $options->editTexts,
            'motive' => $options->motive,
            'details' => array_map(function ($detailOptions) {
                $detail = [
                    'documentDetailId' => $detailOptions->documentDetailId,
                    'unitValue' => $detailOptions->unitValue,
                    'quantity' => $detailOptions->quantity,
                ];

                return $detail;
            }, $options->details),
        ];

        $client = [
            'code' => $options->clientRut,
            'activity' => $options->clientActivity,
        ];

        if (null !== $options->clientName) {
            $client['company'] = $options->clientName;
        }
        if (null !== $options->clientCity) {
            $client['city'] = $options->clientCity;
        }
        if (null !== $options->clientAddress) {
            $client['address'] = $options->clientAddress;
        }
        if (null !== $options->clientMunicipality) {
            $client['municipality'] = $options->clientMunicipality;
        }

        $optionsArr['client'] = $client;

        if (null !== $options->emissionDateTimestamp) {
            $optionsArr['emissionDate'] = $options->emissionDateTimestamp;
        }

        if (null !== $options->expirationDateTimestamp) {
            $optionsArr['expirationDate'] = $options->expirationDateTimestamp;
        }

        if (null !== $options->documentTypeId) {
            $optionsArr['documentTypeId'] = $options->documentTypeId;
        } elseif (null !== $options->documentSiiCode) {
            $optionsArr['codeSii'] = $options->documentSiiCode;
        } else {
            throw new \Exception('When creating a refund, one of documentTypeId or documentSiiCode should be != null');
        }
        
        //para que se notifique al servicio
        $optionsArr['declareSii'] = 1;

        $request = $this->httpClient->post('/v1/returns.json', ['body' => json_encode($optionsArr)]);
        $res = (string) $request->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }
}
