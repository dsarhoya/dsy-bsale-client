<?php

namespace DSYBSaleClient\Elements;

use DSYBSaleClient\Elements\Options\Documents as Options;
use DSYBSaleClient\Elements\Options\Documents\CreateDocumentDynamicAtributeOptions;
use DSYBSaleClient\Elements\Options\Documents\CreateDocumentPaymentsOptions;
use DSYBSaleClient\Elements\Options\Documents\CreateInvoiceDocumentOptions;
use DSYBSaleClient\Elements\Options\Documents\CreateInvoiceDocumentReferenceOptions;
use DSYBSaleClient\Exception\BSaleClientException;

/**
 * Documents.
 */
class Documents extends AbstractElements
{
    /**
     * Undocumented function.
     */
    public function getDocument(Options\GetDocumentOptions $options)
    {
        $query = [];
        if (null !== $options->expand) {
            $query['expand'] = '['.(implode(',', $options->expand)).']';
        }
        $res = (string) $this->httpClient->get("/v1/documents/{$options->documentId}.json", ['query' => $query])->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }

    public function getDocumentDetails($documentId)
    {
        return $this->get("/v1/documents/{$documentId}/details.json", ['offset'=> 0, 'limit' => 50], true);
    }

    public function getDocuments(Options\GetDocumentsOptions $options)
    {
        $query['limit'] = $options->limit;
        $query['offset'] = $options->offset;

        if (null !== $options->codeSii) {
            $query['codesii'] = $options->codeSii;
        }

        if (null !== $options->number) {
            $query['number'] = $options->number;
        }

        if (null !== $options->rangeDate) {
            $query['emissiondaterange'] = '['.(implode(',', $options->rangeDate)).']';
        }

        if (null !== $options->officeId) {
            $query['officeid'] = $options->officeId;
        }

        if (null !== $options->expand) {
            $query['expand'] = '['.(implode(',', $options->expand)).']';
        }

        return $this->get('/v1/documents.json', $query, $options->recursive);
    }

    /**
     * IMPORTANTE en Bsale, si no hay cliente asociado al rut, crean el cliente con la información que mandaste. En
     * las llamadas siguiente que se hagan con el mismo cliente, ocupa los datos que tiene guardados (ignora lo que recibe la API).
     *
     * @param CreateInvoiceDocumentOptions $options [description]
     *
     * @return mixed
     */
    public function createInvoiceDocument(Options\CreateInvoiceDocumentOptions $options)
    {
        // if (null !== $options->documentTypeId && null !== $options->documentSiiCode) {
        //     throw new BSaleClientException('There should be only a documentTypeId or documentSiiCode');
        // }
        // if (null === $options->documentTypeId && null === $options->documentSiiCode) {
        //     throw new BSaleClientException('There should be at least a documentTypeId or documentSiiCode');
        // }

        // $client = [
        //     /*
        //      * En caso de agregar el clientId, hay que cambiar la regla que agrega el $client al $optionsArr. Actualmente, si no tiene code no lo agrega, eso habría que cambiarlo.
        //      */
        //     'code' => $options->clientRut,
        // ];

        // if (null !== $options->clientActivity) {
        //     $client['activity'] = $options->clientActivity;
        // }
        // if (null !== $options->clientName) {
        //     $client['company'] = $options->clientName;
        // }
        // if (null !== $options->clientCity) {
        //     $client['city'] = $options->clientCity;
        // }
        // if (null !== $options->clientAddress) {
        //     $client['address'] = $options->clientAddress;
        // }
        // if (null !== $options->clientMunicipality) {
        //     $client['municipality'] = $options->clientMunicipality;
        // }

        // $optionsArr = [
        //     'details' => array_map(function ($detailOptions) {
        //         $detail = [
        //             'netUnitValue' => $detailOptions->netUnitValue,
        //             'quantity' => $detailOptions->quantity,
        //             'comment' => $detailOptions->comment,
        //         ];
        //         if (null !== $detailOptions->taxes) {
        //             $detail['taxes'] = $detailOptions->taxes;
        //         }
        //         if (null !== $detailOptions->discount) {
        //             $detail['discount'] = $detailOptions->discount;
        //         }
        //         if (null !== $detailOptions->code) {
        //             $detail['code'] = $detailOptions->code;
        //         }
        //         if (null !== $detailOptions->variantId) {
        //             $detail['variantId'] = $detailOptions->variantId;
        //         }

        //         return $detail;
        //     }, $options->details),
        // ];

        // /**
        //  * si $client tiene rut, lo agrego.
        //  * En caso de agregar el clientId, esta regla también tendría que considerarlo.
        //  *
        //  * La razón de mandar un $optionsArr sin cliente, es que se puede estar creando una boleta.
        //  */
        // if (null !== $client['code']) {
        //     $optionsArr['client'] = $client;
        // }

        // if (null !== $options->emissionDateTimestamp) {
        //     $optionsArr['emissionDate'] = $options->emissionDateTimestamp;
        // }

        // if (null !== $options->expirationDateTimestamp) {
        //     $optionsArr['expirationDate'] = $options->expirationDateTimestamp;
        // }

        // if (null !== $options->officeId) {
        //     $optionsArr['officeId'] = $options->officeId;
        // }

        // if (null !== $options->salesConditionId) {
        //     $optionsArr['salesConditionId'] = $options->salesConditionId;
        // }

        // if (null !== $options->documentTypeId) {
        //     $optionsArr['documentTypeId'] = $options->documentTypeId;
        // } elseif (null !== $options->documentSiiCode) {
        //     $optionsArr['codeSii'] = $options->documentSiiCode;
        // }

        // if (null !== $options->dispatch && true === $options->dispatch) {
        //     $optionsArr['dispatch'] = 1;
        // }

        // if (null != $options->dynamicAttributes) {
        //     $optionsArr['dynamicAttributes'] = array_map(function (CreateDocumentDynamicAtributeOptions $options) {
        //         return $options->getAsArray();
        //     }, $options->dynamicAttributes);
        // }

        // if (null != $options->payments) {
        //     $optionsArr['payments'] = array_map(function (CreateDocumentPaymentsOptions $options) {
        //         return $options->getAsArray();
        //     }, $options->payments);
        // }

        // if (null != $options->references) {
        //     $optionsArr['references'] = array_map(function (CreateInvoiceDocumentReferenceOptions $options) {
        //         return $options->getAsArray();
        //     }, $options->references);
        // }

        // if (null != $options->sellerId) {
        //     $optionsArr['sellerId'] = $options->sellerId;
        // }

        // //para que se notifique al servicio
        // $optionsArr['declareSii'] = 1;

        $optionsArr = $this->getPostInvoiceBodyArray($options);

        return $this->postInvoiceDocument(json_encode($optionsArr));

        // $request = $this->httpClient->post('/v1/documents.json', ['body' => json_encode($optionsArr)]);
        // $res = (string) $request->getBody();

        // return \GuzzleHttp\json_decode($res, true);
    }

    public function getPostInvoiceBodyArray(Options\CreateInvoiceDocumentOptions $options): array
    {
        if (null !== $options->documentTypeId && null !== $options->documentSiiCode) {
            throw new BSaleClientException('There should be only a documentTypeId or documentSiiCode');
        }
        if (null === $options->documentTypeId && null === $options->documentSiiCode) {
            throw new BSaleClientException('There should be at least a documentTypeId or documentSiiCode');
        }

        $client = [
            /*
             * En caso de agregar el clientId, hay que cambiar la regla que agrega el $client al $optionsArr. Actualmente, si no tiene code no lo agrega, eso habría que cambiarlo.
             */
            'code' => $options->clientRut,
        ];

        if (null !== $options->clientActivity) {
            $client['activity'] = $options->clientActivity;
        }
        if (null !== $options->clientName) {
            $client['company'] = $options->clientName;
        }
        if (null !== $options->clientCity) {
            $client['city'] = $options->clientCity;
        }
        if (null !== $options->clientAddress) {
            $client['address'] = $options->clientAddress;
        }
        if (null !== $options->clientMunicipality) {
            $client['municipality'] = $options->clientMunicipality;
        }

        $optionsArr = [
            'details' => array_map(function ($detailOptions) {
                $detail = [
                    'netUnitValue' => $detailOptions->netUnitValue,
                    'quantity' => $detailOptions->quantity,
                    'comment' => $detailOptions->comment,
                ];
                if (null !== $detailOptions->taxes) {
                    $detail['taxes'] = $detailOptions->taxes;
                }
                if (null !== $detailOptions->discount) {
                    $detail['discount'] = $detailOptions->discount;
                }
                if (null !== $detailOptions->code) {
                    $detail['code'] = $detailOptions->code;
                }
                if (null !== $detailOptions->variantId) {
                    $detail['variantId'] = $detailOptions->variantId;
                }

                return $detail;
            }, $options->details),
        ];

        /**
         * si $client tiene rut, lo agrego.
         * En caso de agregar el clientId, esta regla también tendría que considerarlo.
         *
         * La razón de mandar un $optionsArr sin cliente, es que se puede estar creando una boleta.
         */
        if (null !== $client['code']) {
            $optionsArr['client'] = $client;
        }

        if (null !== $options->emissionDateTimestamp) {
            $optionsArr['emissionDate'] = $options->emissionDateTimestamp;
        }

        if (null !== $options->expirationDateTimestamp) {
            $optionsArr['expirationDate'] = $options->expirationDateTimestamp;
        }

        if (null !== $options->officeId) {
            $optionsArr['officeId'] = $options->officeId;
        }

        if (null !== $options->salesConditionId) {
            $optionsArr['salesConditionId'] = $options->salesConditionId;
        }

        if (null !== $options->documentTypeId) {
            $optionsArr['documentTypeId'] = $options->documentTypeId;
        } elseif (null !== $options->documentSiiCode) {
            $optionsArr['codeSii'] = $options->documentSiiCode;
        }

        if (null !== $options->dispatch && true === $options->dispatch) {
            $optionsArr['dispatch'] = 1;
        }

        if (null != $options->dynamicAttributes) {
            $optionsArr['dynamicAttributes'] = array_map(function (CreateDocumentDynamicAtributeOptions $options) {
                return $options->getAsArray();
            }, $options->dynamicAttributes);
        }

        if (null != $options->payments) {
            $optionsArr['payments'] = array_map(function (CreateDocumentPaymentsOptions $options) {
                return $options->getAsArray();
            }, $options->payments);
        }

        if (null != $options->references) {
            $optionsArr['references'] = array_map(function (CreateInvoiceDocumentReferenceOptions $options) {
                return $options->getAsArray();
            }, $options->references);
        }

        if (null != $options->sellerId) {
            $optionsArr['sellerId'] = $options->sellerId;
        }

        //para que se notifique al servicio
        $optionsArr['declareSii'] = 1;

        return $optionsArr;

    }

    public function postInvoiceDocument(string $json_body)
    {
        $request = $this->httpClient->post('/v1/documents.json', ['body' => $json_body]);
        $res = (string) $request->getBody();

        return \GuzzleHttp\json_decode($res, true);

    }
}
