<?php

namespace DSYBSaleClient\Elements;

use DSYBSaleClient\Elements\Options\Payments\CreatePaymentOptions;

/**
 * Documents.
 */
class Payments extends AbstractElements
{
    public function createPayment(CreatePaymentOptions $options)
    {
        $optionsArr = [
            'recordDate' => $options->recordDate,
            'amount' => $options->amount,
            'documentId' => $options->documentId,
            'paymentTypeId' => $options->paymentTypeId,
        ];

        $request = $this->httpClient->post('/v1/payments.json', ['body' => json_encode($optionsArr)]);
        $res = (string) $request->getBody();

        return \GuzzleHttp\json_decode($res, true);
    }
}
